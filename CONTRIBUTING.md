# How to contribute

## Installing requirements

To install the requirements needed for development, run `pip install -r requirements/requirements-dev.txt`.
The `requirements/requirements-*.txt` files are built via `pip-compile` which is available once `requirements/requirements-dev.txt` is installed.
New requirements are added to the corresponding file in `requirements.in` (e.g. `requirements.in/requirements.txt` for requirements needed to import **pycontracts-types**) and the actual file is then produced via `pip-compile` (e.g. `pip-compile --output-file requirements/requirements-test.txt requirements.in/requirements.txt`).

## Running tests

Running `make tests` builds a Docker image and runs `pytest --doctest-modules .` inside the container.

## Building documentation

Running `make docs` builds and runs a Docker image which serves Sphinx documentation at http://localhost:8000.

## Installing the pre-commit hook

**pycontracts-types** uses [pre-commit](https://pre-commit.com/) to easily configure pre-commit hooks.
The configuration is stored in `.pre-commit-config.yaml` and has to be installed via:

```bash
pre-commit install
```

Afterwards the configured checks will run before committing.

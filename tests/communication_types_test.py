import pytest
from pycontracts_types.communication import EmailAddress, PhoneNumber


def test_valid_phonenumber_first():
    """Test whether valid phone numbers are parsed and returned correctly."""
    valid_numbers = [
        "(+431)5438786543",
        "+435438786543",
        "05438786543",
        "0543/8786543",
        "0543/878 65 43",
        "(+43)543/878 65 43",
        "03456 873923/4",
    ]

    for valid_number in valid_numbers:
        assert PhoneNumber.first(valid_number) == valid_number


def test_partially_valid_phonenumbers_first():
    """Test whether strings containing valid phone numbers are parsed and returned."""
    partially_valid = [
        ("(+431)5438786543  ", "(+431)5438786543"),
        (" +435438786543", "+435438786543"),
        ("(05438786543)", "05438786543"),
        ("Phone: 0543/8786543", "0543/8786543"),
        ("0543/878 65 43,(+43)543/878 65 43", "0543/878 65 43"),
    ]

    for raw, parsed in partially_valid:
        assert PhoneNumber.first(raw) == parsed


def test_valid_emails_first():
    """Test whether valid email addresses are parsed and returned correctly."""
    valid_addresses = ["bob@alice.com", "bob@alice.co.uk", "bob.alice@eve.co.uk"]

    for valid_address in valid_addresses:
        assert EmailAddress.first(valid_address) == valid_address


def test_partially_valid_emails_first():
    """Test whether strings containing valid email addresses are parsed and returned."""
    partially_valid = [
        (" bob@alice.com", "bob@alice.com"),
        ("bob@alice.co.uk!", "bob@alice.co.uk"),
        ("Email: bob.alice@eve.co.uk, ", "bob.alice@eve.co.uk"),
    ]

    for raw, parsed in partially_valid:
        assert EmailAddress.first(raw) == parsed

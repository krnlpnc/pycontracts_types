FROM python:3.6 as base

RUN mkdir /app
ADD ./requirements/ /app/requirements/
RUN pip install -r /app/requirements/requirements-dev.txt
RUN pip install -r /app/requirements/requirements-doc.txt


FROM base as doc
ADD . /app
WORKDIR /app/doc/
EXPOSE 8000
ENTRYPOINT ["/bin/bash", "-c", "./serve.py"]


FROM base as test
ADD . /app
WORKDIR /app/

import regex as re
from contracts import new_contract

from .base import Regex


class PhoneNumber(Regex):
    """A phone number that may include a prefix and/or an extension."""

    regex = r"(\(?\+[0-9]{1,3}\)?)?[0-9]+/?([0-9] ?)+/?[0-9]+"
    pattern = re.compile(regex)
    strict_pattern = re.compile("^" + regex + "$")

    @staticmethod
    def check(q: str) -> bool:
        """Checks whether the given string is a valid phone number.

        >>> valid_numbers = [
        ...     '(+431)5438786543', '+435438786543', '05438786543',
        ...     '0543/8786543', '0543/878 65 43', '(+43)543/878 65 43',
        ...     '03456 873923/4',
        ... ]
        >>> all([PhoneNumber.check(valid) for valid in valid_numbers])
        True
        """
        if q:
            return PhoneNumber.strict_pattern.match(q) is not None
        return False


class EmailAddress(Regex):
    """An email address."""

    # Regex from https://emailregex.com/
    regex = r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"
    pattern = re.compile(regex)
    strict_pattern = re.compile("^" + regex + "$")

    @staticmethod
    def check(q: str) -> bool:
        """Checks whether the given string is a valid email address.

        >>> valid_addresses = ['bob@alice.com', 'bob@alice.co.uk']
        >>> all([EmailAddress.check(valid) for valid in valid_addresses])
        True
        """
        if q:
            return EmailAddress.strict_pattern.match(q) is not None
        return False


new_contract("phone", PhoneNumber.check)
new_contract("email", EmailAddress.check)

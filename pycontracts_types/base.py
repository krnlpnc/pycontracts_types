from abc import ABC, abstractmethod
from typing import Optional


class Regex(ABC):
    """A base class for types that are based on regex matching.

    .. warning::
       All subclasses must implement the members:
       * :attr:`regex` which is a regex string without :code:`^` and :code:`$`, and
       * :attr:`pattern` that is a compiled Regular Expression Object of :code:`regex`.
    """

    @staticmethod
    @abstractmethod
    def check(q: str) -> bool:
        """Check whether the given string conforms to the regex pattern.

        :param q:   A string containing input data.
        :returns:   :code:`True` if :attr:`q` conforms to the classes regex expression.
        """
        pass

    @classmethod
    def first(cls, q: str) -> Optional[str]:
        """Return the first regex match.

        :returns:   The first match of the regex pattern, or :code:`None`.
        """
        match = cls.pattern.search(q)
        if match:
            return match.group()

import regex as re
from contracts import new_contract

from .base import Regex


class Initial(Regex):
    """A single initial.

    A single Unicode letter followed by an optional dot.

    >>> Initial.check('A.')
    True
    >>> Initial.check('A')
    True
    """

    regex = r"\p{L}\.?"
    pattern = re.compile("^" + regex + "$")

    @staticmethod
    def check(q: str) -> bool:
        return Initial.pattern.match(q) is not None


class Name:
    """A person's name.

    Multiple Unicode letters concatenated with optional dash, tick or space to
    allow for compound names.

    >>> Name.check('Beazley')
    True
    >>> Name.check('Anne-Marie')
    True
    >>> Name.check("O'Brian")
    True
    >>> Name.check('De Cordova')
    True
    """

    regex = r"\p{L}+([' -]\p{L}+)*"
    pattern = re.compile("^" + regex + "$")

    @staticmethod
    def check(q: str) -> bool:
        return Name.pattern.match(q) is not None


class Names:
    """Multiple names concatenated with spaces.

    Multiple :class:`Name`  or :class:`Initial` concatenated with spaces.

    >>> Names.check('Rosa Parks')
    True
    >>> Names.check('Amplif Y.')
    True
    >>> Names.check('D. R. E.')
    True
    >>> Names.check('David T. C.')
    True
    >>> Names.check("David O'Brian")
    True
    >>> Names.check('David T. C. Jones-Summers')
    True
    """

    @staticmethod
    def check(q: str) -> bool:
        names = q.split(" ")
        if len(names) > 1:
            return all([Name.check(n) or Initial.check(n) for n in names])
        return False


initial = new_contract("initial", Initial.check)
name = new_contract("name", Name.check)
names = new_contract("names", Names.check)

first_name = new_contract("first_name", lambda q: Name.check(q) or Names.check(q))
last_name = new_contract("last_name", Name.check)
salutation = new_contract("salutation", Names.check)

test:
	find . -name "*.pyc" -delete
	docker build --target test -t krnlpnc/pycontracts-types-test .
	docker run -t --rm krnlpnc/pycontracts-types-test pytest --doctest-modules .

docs:
	find . -name "*.pyc" -delete
	docker build --target doc -t krnlpnc/pycontracts-types-doc .
	docker run --rm -p 8000:8000 krnlpnc/pycontracts-types-doc

import setuptools

with open("README.md", "r") as fh:
    readme = fh.read()

setuptools.setup(
    name="pycontracts_types",
    version="0.0.1",
    author="krnlpnc",
    description="Additional contracts for pycontracts.",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/krnlpnc/pycontracts_types",
    packages=setuptools.find_packages(),
    install_requires=["pycontracts>=1.8.7", "regex>=2018.11.22"],
    classifiers=["Programming Language :: Python :: 3"],
)

Reference
===========================

.. automodule:: pycontracts_types
   :members:

Base types
----------

Contains abstract types that other types can build upon.

.. automodule:: pycontracts_types.base
   :members:

Communication types
-------------------

Types relating to communication, such as e.g. E-mail addresses or phone numbers.

.. automodule:: pycontracts_types.communication
   :members:
   :inherited-members:

Name types
----------

Types relating to person's names.

.. automodule:: pycontracts_types.names
   :members:
   :inherited-members:

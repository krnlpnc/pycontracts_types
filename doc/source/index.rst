.. pycontracts-types documentation master file, created by
   sphinx-quickstart on Sat Jan  5 21:32:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pycontracts-types's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   reference


**pycontracts_types** is a collection of contracts for `pycontracts`_, a run-time constraints checker for function parameters and return values.
The standard `pycontracts`_ module supports constraint checking for standard Python classes and those defined in your code.
In some cases, e.g. constraint checking a parser that interacts with some API, common types that do not respond to any Python class are very useful.
This package aims to collect such types, such as e.g. email addresses or phone number found in :mod:`pycontracts_types.communication`.

.. _pycontracts: https://github.com/AndreaCensi/contracts


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

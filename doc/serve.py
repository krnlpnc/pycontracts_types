#!/usr/bin/env python3
"""Compiles and serves Sphinx documentation."""

import http.server
import os
import socketserver
from subprocess import call

call(["echo", "Building documentation..."])
if call(["make", "html"]) == 0:
    call(["echo", "~~~~~~~~~~~~~~~~SUCCESS~~~~~~~~~~~~~~~~~~~~~~"])
    os.chdir("./build/html/")

    PORT = 8000
    Handler = http.server.SimpleHTTPRequestHandler

    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        call(["echo", "        Serving docs @ http://localhost:{}".format(PORT)])
        call(["echo", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"])
        httpd.serve_forever()
